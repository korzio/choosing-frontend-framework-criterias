# Choosing Frontend Framework - Criterias

This note is intended to help choosing a frontend framework. However it can't give you exact answer which library to choose, it considers different aspects and proposes criterias to review alternatives.

> Premature optimization is the root of all evil - [Donald Knuth](https://en.wikiquote.org/wiki/Donald_Knuth)

## The problem

A [list of existing frontend frameworks](https://github.com/showcases/front-end-javascript-frameworks) is huge and it is being updated every day (hour). It is impossible to know them all, so how can we compare them?

## Goals

- Review different approaches
- Define valuable criterias

## Project requirements

Understanding the project you are working on can give you many answers to framework related questions.

- What is the product? What is the app you are gonna build?

If you are building a browser extension or some product which will be injected into other sites, you surely want to keep your styles untouched and not to touch 3rd party styles as well. So the style encapsulation criteria is important in this case.
On the other hand, if you want to build an application for TV, then the total application size and speed are most important metrics.
Or maybe you are working on another framework which will be used by teams of developers to build banking applications. In this case, relevant factors can be maintainability and clean components architecture.

- Speed, Quality, Support?

How fast should it be delivered? If the answer is 'yesterday', then skip this article and just start programming. Well, if it needs to be delivered fast, you'll probably want to use the most acquainted framework.

- Should it be server side rendered?

    - Does it need to be indexed by search engines?
    - Will it be a SPA?

Some frameworks do not support server-side rendering, so it can be a crucial point.

- Who are the clients?

    - Is if for internal usage or not?
    - What is the necessary browser support?
    - Does it have custom limitations or specifications to use?
    - What are the requirements regarding accessibility and internalization?

- [Mobile first](https://www.thinkwithgoogle.com/interviews/think-mobile-first.html)? Responsive?

Mobile and tablets are the most popular devices to browse the Web. Will your project have a native application or it should be responsive?

- Codebase

If the codebase exists, it makes sense at least to keep it in mind. Part of it could be refactored or reused.

- Who is the team?

Depending on a team experience you can understand better, how much time and effort it will take to start with a new framework or with the known one.
Frameworks that are easy to learn, and are well documented have more weight.

## Statistics

Latest browsers statistics can play a big role in a project architecture decisions:

- [Mobile Platforms ~50%](http://gs.statcounter.com/#all-comparison-ww-monthly-201508-201608)
- [Chrome Browser ~50%](http://gs.statcounter.com/#all-browser-ww-monthly-201508-201608)
- [Average JS Size ~400 kB](http://httparchive.org/trends.php#bytesJS&reqJS)
- [Average Site Size ~2.5 mB](http://httparchive.org/trends.php#bytesTotal&reqTotal)

This resources can help understand modern trends:

- [StatCounter](http://gs.statcounter.com/)
- [W3Counter](https://www.w3counter.com/globalstats.php)
- [HTTP Archive](http://httparchive.org/)
- [W3Schools](http://www.w3schools.com/browsers/default.asp)

## Infrastructure

As everyone loves [TDD](https://en.wikipedia.org/wiki/Test-driven_development), first think about [test framework](http://stackoverflow.com/questions/300855/javascript-unit-test-tools-for-tdd) and an infrastructure.

There are a lot of nice articles on how to write tests:

- [How to Write Better Tests](https://medium.com/javascript-scene/what-every-unit-test-needs-f6cd34d9836d) - [Eric Elliott](https://medium.com/@_ericelliott)
- [Testing framework comparison](https://www.codementor.io/javascript/tutorial/javascript-testing-framework-comparison-jasmine-vs-mocha) - [David Tang](http://thejsguy.com/)

The most popular tools are:

- [Karma](http://karma-runner.github.io/1.0/index.html) - test runner, can be used with different testing frameworks
- [Mocha](https://mochajs.org/) - runner and also a testing framework, doesn't have assertion utilities
- [Jasmine](http://jasmine.github.io/edge/introduction.html) - a testing framework
- [Protractor](http://www.protractortest.org/#/) - end-to-end tests for Angular JS applications, uses [Selenium Web Driver](https://github.com/SeleniumHQ/selenium/wiki/WebDriverJs)

feature | karma | mocha | jasmine
-- | -- | -- | --
runner | Y | Y | N
bdd | N | Y | Y
assertions | N | N | Y
async | N | Y | Y
spy | N | N | Y

Other instruments:

- [Chai](http://chaijs.com/) - a BDD / TDD assertion library
- [jsverify](http://jsverify.github.io/) - a property-based testing library
- [Sinon](http://sinonjs.org/) - spies, mocks library
- [Istanbul](https://github.com/gotwarlost/istanbul) - code coverage tool

### Lint

Linting code helps to keep a source clean, easy to read, avoid syntax errors.

- [Eslint](http://eslint.org/) - popular linting tool
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) - common linting settings

### Organize code

The way of how to organize a code and a workflow is a out of scope of this overview, however this question is actual for every project.

- [Node.js Project Structure Tutorial](https://blog.risingstack.com/node-hero-node-js-project-structure-tutorial/)
- [A Better File Structure For React/Redux Applications](http://marmelab.com/blog/2015/12/17/react-directory-structure.html)
- [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/centralized-workflow)
- [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)

### Package Manager

Right questions to ask before choosing a package manager is:

- What do we deliver?
- Who are our customers (again)?

feature | [npm](https://docs.npmjs.com/files/package.json#dependencies) | [bower](https://bower.io/docs/api/#install) | [jspm](http://jspm.io/docs/installing-packages.html)
-- | -- | -- | --
git resolver | Y | Y | Y
npm resolver | Y | N | Y
bower resolver | N | Y | N
url resolver | Y | Y | Y
local resolver | Y | Y | Y
custom resolver | N | Y | Y
lock dependencies | Y | Y | Y

### Bundle

Almost every project has to bundle resources to decrease download time and [optimize resources size](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/optimize-encoding-and-transfer?hl=en).

Basic bundler features are

- code - resolve file dependencies, in [es6 modules](http://exploringjs.com/es6/ch_modules.html), [commonjs](http://www.commonjs.org/), [amd](https://github.com/amdjs/amdjs-api/wiki/AMD)
- [transpilers](https://webpack.github.io/docs/using-loaders.html)
- plugins
- styles - resolve file dependencies, transpile to css

Most of the bundlers support all this features (and [many more](https://webpack.github.io/docs/comparison.html)), either out of the box or with plugins

- [webpack](https://webpack.github.io/)
- [browserify](http://browserify.org/)
- [gulp](http://gulpjs.com/)
- [grunt](http://gruntjs.com/)

Gulp and Grunt are task runners, but their popular usage is bundling.

## Language

The main language for scripting for browsers is JavaScript. That does not mean that is impossible to develop on other [programming languages](https://www.quora.com/Why-is-JavaScript-the-only-client-side-language-available), but JavaScript is the most popular.
Relying on a defined browser support for the project, [transpilers](https://babeljs.io/#babel-transforms) are used to translate one language standard to another. [ES6 edition](http://www.ecma-international.org/ecma-262/6.0/#sec-imports) is not supported fully by browsers and NodeJS as well, so it also should be transpiled in most cases.

- [TypeScript](https://www.typescriptlang.org/) - is strictly typed superset of JavaScript
- [Elm](http://elm-lang.org/) - functional language compiled to JavaScript
- [WebAssembly](https://webassembly.github.io/) - a subset of JavaScript, which is highly optimized in browsers

# Frameworks

Good way to start is to watch [Rob Eisenberg's talk - Choosing a JavaScript Framework](https://www.youtube.com/watch?v=6I_GwgoGm1w)

- [React](https://facebook.github.io/react/)
- [AngularJS 1 and Angular 2](https://angularjs.org/)
- [Polymer](https://www.polymer-project.org/1.0/)
- [Ember](http://emberjs.com/)
- [Aurelia](http://aurelia.io/)
- [Elm](http://elm-lang.org/)
- [Cycle.js](http://cycle.js.org/)
- [Riot](http://riotjs.com/)
- [ExtJS](https://www.sencha.com/products/extjs/)
- [Vue.js](http://vuejs.org/)
- ...

## Quality

The ISO standard for [System and software quality models](http://www.iso.org/iso/catalogue_detail.htm?csnumber=35733) can be used as a basis for a framework quality evaluation.

> **Quality in use** is the degree to which a product or system can be used by specific users to meet their needs to achieve specific goals with effectiveness, efficiency, freedom from risk and satisfaction in specific contexts of use.

> - Effectiveness - accuracy and completeness with which users achieve specified goals
> - Efficiency - resources expended in relation to the accuracy and completeness with which users achieve goals
> - Satisfaction - degree to which user needs are satisfied when a product or system is used in a specified context of use
> - Freedom from risk - degree to which a product or system mitigates the potential risk to economic status, human life, health, or the environment
> - Context coverage - degree to which a product or system can be used with effectiveness, efficiency, freedom from risk and satisfaction in both specified contexts of use and in contexts beyond those initially explicitly identified

> **Maintainabiliy** is the degree of effectiveness and efficiency with which a product or system can be modified by the intended maintainers

> - Modularity - degree to which a system or computer program is composed of discrete components such that a change to one component has minimal impact on other components
> - Reusability - degree to which an asset can be used in more than one system, or in building other assets
> - Analysability - degree of effectiveness and efficiency with which it is possible to assess the impact on a product or system of an intended change to one or more of its parts, or to diagnose a product for deficiencies or causes of failures, or to identify parts to be modified
> - Modifiability - degree to which a product or system can be effectively and efficiently modified without introducing defects or degrading existing product quality
> - Testability - degree of effectiveness and efficiency with which test criteria can be established for a system, product or component and tests can be performed to determine whether those criteria have been met

Other criterias might be considered in a context of quality as well.

## Common Criterias

- Size

> It's important to understand how big  a download each of these frameworks is and what you are getting for that extra weight in your application.  The size affects performance but also gives you an indication of how ambitious a framework is and how long it might take you to learn this new technology as well as hint at how many ways its trying to help you build your application (i.e. how many features does it support and how robust are they).  The more ambitious and feature rich a framework is the harder it will typically be to integrate it with others particularly on the same page of an app.  Lighter frameworks are more like a library and a smaller commitment to include it in your project. - [Craig McKeachie - Choosing a JavaScript MVC Framework](http://www.funnyant.com/choosing-javascript-mvc-framework/)

The basic framework size, but also a list of popular plugins, which your application will probably contain. In many cases if you choose AngularJS 1, you also want to choose [ui-router](http://ngmodules.org/modules/ui-router) package. Or in case of React it can be some [React + Redux](http://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html) bundle.

- [Speed](http://mathieuancelin.github.io/js-repaint-perfs/)

Obviously, one of the most [significant parameters](https://github.com/krausest/js-framework-benchmark). It is required for the best user experience to have the minimal action latency. This includes also loading, [rendering](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/analyzing-crp?hl=en) time.

- Popularity and community size (Popular on Stackoverflow, Github, number of related packages, etc...)

> The more well-known and recognized the framework is, the more it will be "living," evolving and complete: new ideas, the number and quality of plug-ins, etc. - [Symfony - 10 criteria for choosing the correct framework](http://symfony.com/ten-criteria)

- Support

Documentation, tutorials, trainings, best practices, other tools surrounding framework define how easy it is to learn, how fast it is possible to start development.

- License
- Language
- Browsers Support

## Philosophy Criterias

- Modules / Components system

Means the way how to organize a code inside the application. Both patterns are about keeping units reusable, encapsulate logic and implementation, cover them with tests.
[Module pattern](https://addyosmani.com/resources/essentialjsdesignpatterns/book/#modulepatternjavascript)
[Component based development](http://benmccormick.org/2014/08/07/component-based-development/)

- Templates

Some frameworks has internal templating functional. React has a jsx language (transpiled to JavaScript) to use templates inside views.

- DI

A software design pattern which allows to manage module dependencies to facilitate a developer live. AngularJS has built-in implementation of a DI pattern.

- Virtual Dom

Virtual Dom is a modern way to re-render only part of a changed DOM-tree instead of the whole document/view. This technique helps a browser to speed up user interface.

- Standards Compliance

A framework might be using its self solution for any feature, which actually is supposed to be a standard. This eventually means that in some moment browser implementation will exist.
Metrics mentioned in a [Rob Eisenberg's talk](https://www.youtube.com/watch?v=6I_GwgoGm1w):

1. Aurelia: HTML, ES 2016, Web Components
2. Polymer: HTML, ES 2015, Web Components
3. Ember: HTML, ES 2015
4. Angular 2: ES 2016 (TypeScript). Non Compliant: NG2 Markup and Dart
5. Angular 1: HTML, ES5. Non Compliant: Modules, Dependency Injection
6. React: ES 2015. Non Compliant: JSX

This measurement can also be splitted into different sub-categories, possibly naming all engaged standarts inside (Web Components, CSS Animation, ...)

## Technical Features Criterias

Sencha in [4 Questions to Ask Before Choosing a JavaScript Framework](https://www.sencha.com/blog/4-questions-to-ask-before-choosing-a-javascript-framework/) article proposed technical features to compare across frameworks

![Sencha Framework Features](https://cdn.sencha.com/img/20141001-analyzing-the-roi-of-javascript-in-enterprise-software-development/modern-web-stack-criteria-1.jpg)

- Interface Elements - Basic - Buttons, Text Fields
- Interface Elements - Compounds Widgets - Grids, Trees
- Interface Elements - Charts
- Interface Elements - Containers, Modals
- Interface Elements - Themes, Styles
- View - Layout manager
- View - Interactions, Drag n Drop
- View - Animation
- View - Templating
- View - Localization
- Logic & Data - State Manager, History, Routes
- Logic & Data - Data Binding
- Logic & Data - Code Structure Distribution, Namespaces, Components and Modular approaches
- Logic & Data - Testing
- Logic & Data - Stores, Validation
- Logic & Data - Transport (AJAX, WebSockets)
- Logic & Data - Progressive App - Might be an additional requirement for the project

## Business Criterias

[Rob Eisenberg's talk](https://www.youtube.com/watch?v=6I_GwgoGm1w) measurements for business:

- Corporate Buy-in
- Commercial Support
- Consulting
- Customer Relations
- Official Relations
- Official Partners
- Motivation
- Brand Recognition

This is a **Satisfaction** and **Freedom from risk** part of Quality in use

## Libraries

[What is the difference between a library and a framework?](http://martinfowler.com/bliki/InversionOfControl.html)

> Inversion of Control is a key part of what makes a framework different to a library.

> A **library** is essentially a set of functions that you can call, these days usually organized into classes. Each call does some work and returns control to the client.

> A **framework** embodies some abstract design, with more behavior built in. In order to use it you need to insert your behavior into various places in the framework either by subclassing or by plugging in your own classes. The framework's code then calls your code at these points.

The lack of functional in some frameworks can be compensate by libraries

- [Flow](https://flowtype.org/) - a static type checker for javascript
- [Ramda](http://ramdajs.com/) / [Lodash](https://lodash.com/) - functional programming utilities
- [JQuery](https://jquery.com/) - DOM, AJAX, animation, utilities helpers
- [Redux](http://redux.js.org/) - a CQRS state container implementation
- [Immutable](https://facebook.github.io/immutable-js/docs/#/) / [Seamless-immutable](https://github.com/rtfeldman/seamless-immutable)- immutabiliy implementation
- [Reactive Programming](https://github.com/Reactive-Extensions/RxJS) - Observable pattern
- Loaders - [SystemJS](https://github.com/systemjs/systemjs),...
- Templates - [Nunjucks](https://mozilla.github.io/nunjucks/),...
- Charts / Graphics - [D3](https://d3js.org/),...
- Animation - [GSAP](http://greensock.com/)

## Vanilla

It is not necesarry to use a framework for building a frontend application.

- [Custom Elements](https://medium.com/dev-channel/the-case-for-custom-elements-part-1-65d807b4b439#.ax4sg99yc), Web Components - allow component based approach
- [Shadow Dom](http://w3c.github.io/webcomponents/spec/shadow/) - encapsulate styles
- HTML import - modular / components system
- ES6 template literals - template engine
- ...

## Styles

### Languages

- CSS
- [Less](http://lesscss.org/), [Sass](http://sass-lang.com/), SCSS, [Stylus](http://stylus-lang.com/) - CSS extensions
- [Jss](https://github.com/cssinjs/jss) - A lib for generating CSS from JavaScript

### Style encapsulation

CSS doesn't have scopes, so there are different approaches to avoid styles mash-up.

- Web Components with Shadow DOM in case of [Polymer](https://www.polymer-project.org/1.0/) and [Angular 2](http://blog.thoughtram.io/angular/2015/06/29/shadow-dom-strategies-in-angular2.html).
- [BEM](https://en.bem.info/methodology/) - block element modifier
- **css-in-js** techniques like [CSS Modules](https://github.com/css-modules/css-modules) and [others](https://medium.com/seek-ui-engineering/the-end-of-global-css-90d2a4a06284)

### Libraries

- [Reset.css vs Normalize.css](http://nicolasgallagher.com/about-normalize-css/)

### Design

Popular [styleguides](http://styleguides.io/) are already implemented by JavaScript libraries, so it can be a point to choose some framework

- [Bootstrap](http://getbootstrap.com/)
- [Material](https://material.google.com/)
- ...

## What's next?

This note immerse a reader into a set of frontend terms, techiniques, links. Next aim is to organize resources into usefull conditions, which will help a developer to make decisions.

## References

- [Vue Frameworks Comparison](https://vuejs.org/v2/guide/comparison.html)
- [Frameworks Speed Comparison](http://www.stefankrause.net/js-frameworks-benchmark7/table.html)
- [Web Components](http://www.html5rocks.com/en/tutorials/webcomponents/shadowdom-201/#toc-style-scoped)
- [Radium](http://stack.formidable.com/radium/) - encapsulate styles
- [Angular 2 versus React: There Will Be Blood](https://medium.freecodecamp.com/angular-2-versus-react-there-will-be-blood-66595faafd51#.olgl72yh5)
- [Web Fundamentals - Google Developers](https://developers.google.com/web/fundamentals/?hl=en)